/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   units.h                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <chamada@student.le-101.fr>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2020/02/09 17:43:33 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2020/03/11 16:09:14 by chamada          ###   ########lyon.fr   */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef UNITS_H
# define UNITS_H

int	test_ft_strlen(void);
int	test_ft_strcpy(void);
int	test_ft_strcmp(void);
int	test_ft_write(void);
int	test_ft_read(void);
int	test_ft_strdup(void);

int	test_ft_atoi_base(void);
int	test_ft_list_push_front(void);
int	test_ft_list_size(void);
int	test_ft_list_remove_if(void);

#endif
